#!/bin/bash
#stock image mkbootimg --base 0 --pagesize 2048 --kernel_offset 0x00008000 --ramdisk_offset 0x02900000 --second_offset 0x00f00000 --tags_offset 0x02700000 --cmdline 'console=ttyHSL0,115200,n8 androidboot.hardware=hammerhead user_debug=31 maxcpus=2 msm_watchdog_v2.enable=1' --kernel kernel --ramdisk ramdisk.cpio.gz -o /home/ubuntu/hammerhead-lmy48i/boot.img
#buildroot image mkbootimg --base 0 --pagesize 2048 --kernel_offset 0x00008000 --ramdisk_offset 0x02900000 --second_offset 0x00f00000 --tags_offset 0x02700000 --cmdline 'console=ttyHSL0,115200,n8 androidboot.hardware=hammerhead user_debug=31 maxcpus=2 msm_watchdog_v2.enable=1' --kernel $BASEDIR/build/images/kernel --ramdisk $BASEDIR/build/images/ramdisk.cpio.gz -o $BASEDIR/build/images/buildroot-boot.img
#( set -o posix ; set ) | less
set -o nounset

$BASE_DIR/../external/scripts/mkbootimg --base 0 --pagesize 2048 --kernel_offset 0x00008000 --ramdisk_offset 0x02900000 --second_offset 0x00f00000 --tags_offset 0x02700000 --cmdline 'console=ttyHSL0,115200,n8 androidboot.hardware=hammerhead user_debug=31 maxcpus=2 msm_watchdog_v2.enable=1' --kernel $BASE_DIR/images/zImage --ramdisk $BASE_DIR/images/rootfs.cpio.gz --id -o $BASE_DIR/images/buildroot-boot.img

